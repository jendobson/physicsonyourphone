require 'spec_helper'

describe "DataSetPages" do
  
  subject { page }
  
  let(:user) { FactoryGirl.create(:user) }
  before { sign_in user }
  
  describe "data_set creation " do
    before { visit root_path }
    
    describe "with invalid information" do
      
      it "should not create a data_set" do
        expect { click_button "Post" }.not_to change(DataSet, :count)
      end
      
      describe "error messages" do
        before { click_button "Post" }
        it { should have_content('error') }
      end
    end

    describe "with valid information" do

      describe "using data input form"
      before do
        fill_in 'data_set_description',   with: "Description of Dataset"
        fill_in 'data_set_tdata',         with: "0, 1, 2, 3, 4, 5, 6, 7, 8, 9"
        fill_in 'data_set_ydata',         with: "1, 2, 3, 4, 5, 5, 4, 3, 2, 1"
      end
      it "should create a dataset" do
        expect { click_button "Post" }.to change(DataSet, :count).by(1)
      end
      describe "data display page" do
        before { click_button "Post" }
        it { should have_selector('title', text:"Data") }
        it { should have_selector('h1', text:"Description of Dataset") }
        it { should have_content("0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0") }
        it { should have_content("1.0, 2.0, 3.0, 4.0, 5.0, 5.0, 4.0, 3.0, 2.0, 1.0") }
      end

      ##
      #describe "using filename input" do
      #  before do
      #    fill_in 'data_file_name', with: "~/Work/physics_on_your_phone/spec/testdata/accelerometer_jumping.csv"
      #  end
      #  it "should create a dataset" do
      #    expect { click_button "Upload" }.to change(DataSet, :count).by(1)
      #  end
      #end
      
    end
    
  end

          
            
    
  
  describe "data_set display" do
    let(:user) { FactoryGirl.create(:user) }
    let!(:ds1)  { FactoryGirl.create(:data_set, user: user, description: "First Test Dataset", 
      ydata: [1,2,3,4,5,5,4,3,2,1].pack("f*"), tdata: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].pack("f*") ) }
    
    
    before do
     sign_in user
     visit data_set_path(ds1)
    end

    it { should have_selector('h1',    text: ds1.description) }
    it { should have_selector('title', text: "Data") }

    describe "datasets" do
     it { should have_content(ds1.tdata.unpack("f*")) }
     it { should have_content(ds1.ydata.unpack("f*")) } 
    end

  end
    
end