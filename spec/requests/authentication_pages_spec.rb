require 'spec_helper'

describe "Authentication" do

  subject { page }

  describe "when user is not signed in" do
    before { visit root_path  }
    it { should_not have_link('Settings') }
    it { should_not have_link('Profile') }
  end
  
  describe "signin page" do
    before { visit signin_path }

    it { should have_selector('h1',    text: 'Sign in') }
    it { should have_selector('title', text: 'Sign in') }
  end


  
  describe "signin" do
    before { visit signin_path }

    describe "with invalid information" do
      before { click_button "Sign in" }

      it { should have_selector('title', text: 'Sign in') }
      it { should have_selector('div.alert.alert-error', text: 'Invalid') }

      # Bug fix:  when using flash (not flash.now) to store error information, the flash message
      # persists after the first request.  We don't want the message to persist
      # that long.  (See rails tutorial code listing 8.10 and 8.11.)
      describe "after visiting another page" do
        before { click_link "Home" }
        it { should_not have_selector('div.alert.alert-error') }
      end

    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }

      it { should have_selector('title', text: user.name) }
      it { should have_link('Users',    href: users_path) }

      it { should have_link('Profile', href: user_path(user)) }
      it { should have_link('Settings', href: edit_user_path(user)) }
      it { should have_link('Sign out', href: signout_path) }
      it { should_not have_link('Sign in', href: signin_path) }

      describe "after visiting another page" do
        it { should have_content "You are signed in as #{user.name}." }
        it { should have_link("#{user.name}", href:user_path(user)) }
      end

    end
  end

  describe "authorization" do

    describe "for non-signed-in user" do
      let(:user) { FactoryGirl.create(:user) }

  
      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user)
          sign_in user
        end

        describe "after signing in" do

          it "should render the desired protected page" do
            page.should have_selector('title', text: 'Edit user')
          end
          
          
          describe "when signing in again" do
            before do
              delete signout_path
              visit root_path
              sign_in user
            end
            
            it "should render the default (profile) page" do
              page.should have_selector('title', text: user.name)
            end
            
          end
                    
        end
      end

      describe "in the Users controller" do

        describe "visiting the edit page" do
          before { visit edit_user_path(user) }
          it { should have_selector('title', text: 'Sign in') }
        end

        describe "submitting to the update action" do
          before { put user_path(user) }
          specify { response.should redirect_to(signin_path) }
        end

        describe "visiting the user index" do
          before { visit users_path }
          it { should have_selector('title', text: 'Sign in') }
        end
        
        describe "visiting a specific user page" do
          before { visit user_path(user) }
          it { should have_selector('title', text: 'Sign in') }
        end
      end

      describe "in the datasets controller" do
        
        describe "submitting to the create action" do
          before { post data_sets_path }
          specify { response.should redirect_to(signin_path) }
        end
        
        describe "submitting to the destroy action" do
          before { delete data_set_path(FactoryGirl.create(:data_set)) }
          specify { response.should redirect_to(signin_path) }
        end
        
      end
    end

    describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { sign_in user }

      describe "visiting Users#edit page" do
        before { visit edit_user_path(wrong_user) }
        it { should_not have_selector('title', text: full_title('Edit user')) }
      end

      describe "submitting a PUT request to the Users#update action" do
        before { put user_path(wrong_user) }
        specify { response.should redirect_to(root_path) }
      end
    end

    describe "as non-admin user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:non_admin) { FactoryGirl.create(:user) }
      before { sign_in non_admin }

      describe "submitting a DELETE request to the Users#destroy action" do
        before { delete user_path(user) }
        specify { response.should redirect_to(root_path) }
      end
    end
    
    describe "as admin user" do
      let(:admin) { FactoryGirl.create(:admin) }
      before { sign_in admin }
 
      it "should not be able to delete itself" do
        expect { delete user_path(admin) }.to_not change{ User.count } 
        # Other syntaxes for the delete method don't work, such as
        #    delete :destroy, id:admin
        #    delete :destroy, :id=>admin
      end

      describe "attempt to delete should redirect to index view"  do
        before { delete user_path(admin) }
        specify { response.should redirect_to(users_path) }
      end    
    end
    
    describe "as a signed-in user" do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }
      
      describe "accessing Users#new action" do
        before { visit signup_path }
        it { should_not have_selector('title', text: full_title('Sign up')) }
      end
      
      describe "accessing Users#create action" do
        before { post 'users#create' }
        specify { response.should redirect_to(root_path) }
      end
    end
    
  end

  describe "index" do
      before do
        sign_in FactoryGirl.create(:user)
        FactoryGirl.create(:user, name: "Bob", email: "bob@example.com")
        FactoryGirl.create(:user, name: "Ben", email: "ben@example.com")
        visit users_path
      end

      it { should have_selector('title', text: 'All users') }
      it { should have_selector('h1',    text: 'All users') }

      it "should list each user" do
        User.all.each do |user|
          page.should have_selector('li', text: user.name)
        end
      end
    end
    
    
end
