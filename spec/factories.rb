FactoryGirl.define do
  factory :user do
    sequence(:name)  { |n| "Person #{n}" }
    sequence(:email) { |n| "person_#{n}@example.com"}   
    password "foobar"
    password_confirmation "foobar"
  
    factory :admin do
      admin true
    end
  end
  
  factory :data_set do
    description "My dataset"
    ydata [1, 2, 3, 4, 5, 5, 4, 3, 2, 1].pack("c*")
    tdata [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].pack("c*") 
    user
  end
  
end