# == Schema Information
#
# Table name: data_sets
#
#  id          :integer          not null, primary key
#  description :string(255)
#  ydata       :binary
#  tdata       :binary
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe DataSet do
  
  let(:user) { FactoryGirl.create(:user) }
  before do
    ydata = [ 1, 2, 3, 4, 5, 5, 4, 3, 2, 1]
    tdata = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    @dataset = user.data_sets.build(description:"My First Data Set", tdata:tdata.pack("f*"), ydata:ydata.pack("f*")  )
  end
  
  subject { @dataset }
  
  it { should respond_to(:user_id) }
  it { should respond_to(:description) }
  it { should respond_to(:tdata) }
  it { should respond_to(:ydata) }

  its(:user) { should == user }
  it { should be_valid }
  
  
  describe "accessible attributes" do
    it "should not allow access to user_id" do
      expect do
        DataSet.new(user_id: user.id)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
  
  describe "when user_id is not present" do
    before { @dataset.user_id = nil }
    it { should_not be_valid }
  end
  
  describe "with missing data" do
    describe "ydata" do
      before { @dataset.ydata = nil }
      it { should_not be_valid }
    end
    describe "tdata" do
      before { @dataset.tdata = nil }
      it { should_not be_valid }
    end
  end
  describe "with data that is too long" do
    describe "tdata" do
      before { @dataset.tdata = 0.step(61,0.01).to_a.pack("f*") }
      it { should_not be_valid }
    end
    
    describe "ydata" do
      before { @dataset.ydata  = 0.step(61,0.01).to_a.pack("f*") }
      it { should_not be_valid }
    end
  end
  
end
