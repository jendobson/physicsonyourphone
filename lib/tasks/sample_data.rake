namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    admin = User.create!(name: "Example User",
                 email: "example@railstutorial.org",
                 password: "foobar",
                 password_confirmation: "foobar")
    admin.toggle!(:admin)
    99.times do |n|
      name  = Faker::Name.name
      email = "example-#{n+1}@railstutorial.org"
      password  = "password"
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
    users = User.all(limit: 6)
    10.times do
      tdata = [0,1,2,3,4,5,6,7,8,9].pack("f*");
      ydata = (1..10).map{ 10*rand() }.pack("f*")
      description = Faker::Lorem.sentence(5)
      users.each { |user| user.data_sets.create!(description: description, tdata: tdata, ydata: ydata) }
    end
  end
end