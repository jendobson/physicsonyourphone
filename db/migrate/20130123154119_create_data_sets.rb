class CreateDataSets < ActiveRecord::Migration
  def change
    create_table :data_sets do |t|
      t.string :description
      t.binary :ydata
      t.binary :tdata
      t.integer :user_id
      
      t.timestamps
    end
    
    add_index :data_sets, [:user_id, :created_at]
  end
end
