class DataSetsController < ApplicationController
  before_filter :signed_in_user

  def create
    ydata = params[:data_set][:ydata].split(",").map { |s| s.to_f }.pack("f*")
    tdata = params[:data_set][:tdata].split(",").map { |s| s.to_f }.pack("f*")
    @data_set = current_user.data_sets.build(description: params[:data_set][:description],
      tdata: tdata, ydata: ydata)
    if @data_set.save
      flash[:success] = "Data set created!"
      redirect_to data_set_url(@data_set)
    else
      render 'static_pages/home'
    end
  end

  def destroy
  end
  
  def show
    @data_set = DataSet.find(params[:id])
  end
  
  
end
