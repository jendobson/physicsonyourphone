class StaticPagesController < ApplicationController
  def home
    @data_set = current_user.data_sets.build if signed_in?
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
end
