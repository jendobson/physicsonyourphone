#require "gchart"
#require "gruff"
require "flotilla-rails"

module DataSetHelper
  def graph_for(dataset, options = { size: 300 })
    ydata = dataset[:ydata].unpack("f*")
    tdata = dataset[:tdata].unpack("f*")
    line_width = 10
    color = 'red'
    title = "Chart Title"
    point_size = 3
    #chart_url = Gchart.line(:data => [tdata, ydata], 
    #                          :linewidth => line_width, 
    #                          :line_colors => color, 
    #                          :title => title, 
    #                          :point_size => point_size)
    
    
    chart("graph", { "Store 1" => { :collection => @store_one, :x => :date, :y => :sales } }, { :js_includes => false })
    
    
    image_tag(chart_url, alt: dataset.description, class: "data-graph")
  end
end