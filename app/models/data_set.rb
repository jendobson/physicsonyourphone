# == Schema Information
#
# Table name: data_sets
#
#  id          :integer          not null, primary key
#  description :string(255)
#  ydata       :binary
#  tdata       :binary
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class DataSet < ActiveRecord::Base
  attr_accessible :description, :tdata, :ydata
  belongs_to :user
  
  validates :user_id, presence: true
  
  # Maximum data length is 1 minute at 100 Hz stored as single precision float
  validates :ydata, presence: true, length: { maximum: 4*60*100}
  validates :tdata, presence: true, length: { maximum: 4*60*100}
  
  default_scope order: 'data_sets.created_at DESC'
  
end
